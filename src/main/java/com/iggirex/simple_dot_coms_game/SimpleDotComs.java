package com.iggirex.simple_dot_coms_game;

import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

public class SimpleDotComs {
    final public String RED = "\033[0;31m";
    final public String RESET = "\033[0m";
    final public String YELLOW = "\033[0;33m";
    final public String GREEN_UNDERLINED = "\033[4;32m";
    String GREEN_BRIGHT = "\033[0;92m";

    public Boolean[] setLocations() {
        Boolean[] grid = new Boolean[7];
        Random rand = new Random();
        int startingSpot = rand.nextInt(5);

        for(int i = 0; i <= 7; i++) {
            if (i >= startingSpot && i <= startingSpot + 2) {
                grid[i] = true;
            }
        }
        System.out.println("game grid: " + Arrays.toString(grid));
        return grid;
    }

    public int promptUser() {
        Scanner reader = new Scanner(System.in);
        int n = 0;
        try {
            System.out.println("\nEnter a number from 1 to 7: ");
            n = reader.nextInt() - 1;
            checkUserInput(n);

        } catch(InputMismatchException e) {
            System.out.println(RED + "Input was not an INT!!\n" + e + RESET);
        }
        return n;
    }

    public Boolean checkUserInput(int n) {
        // return is only for testing!
        if (n >= 7 || n < 0) {
            System.out.println(RED + "That number is not in between 1 and 7 DDAAMMMNIITT!!" + RESET);
            return false;
        }
        return true;
    }

    public void runGame(Boolean[] grid) {
        int sunk = 0;
        int turnNumber = 0;
        String skillLevel = "";

        while(sunk < 3) {
            turnNumber++;
            int n = promptUser();

            if(n >= 0 && n < 7 && grid[n] != null) {
                grid[n] = null;
                sunk++;
                System.out.println(GREEN_UNDERLINED + "BOOM! You hit the motherfucker!" + RESET);
            } else {
                System.out.println(YELLOW + "MISSED!!!" + RESET);
            }
        }

        if(turnNumber == 3) {
            skillLevel = "Insane Master of Universe";
        } else if (turnNumber < 4) {
            skillLevel = "Damn Good";
        } else if (turnNumber < 5) {
            skillLevel = "Aight";
        } else if (turnNumber < 7) {
            skillLevel = "Not Very Good";
        } else if (turnNumber >= 7) {
            skillLevel = "WTF?! HORRIBLE!";
        }

        System.out.println(GREEN_BRIGHT + "Finished in " + turnNumber + " turns" + RESET);
        System.out.println("Your skill level is \"" + skillLevel + "\"");
    }
}

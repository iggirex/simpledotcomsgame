package com.iggirex.simple_dot_coms_game;

public class SimpleDotComsApplication {
    public static void main(String[] args) {

        SimpleDotComs simpleDotComs = new SimpleDotComs();

        Boolean[] grid = simpleDotComs.setLocations();
        simpleDotComs.runGame(grid);
    }
}

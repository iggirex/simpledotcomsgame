import com.iggirex.simple_dot_coms_game.SimpleDotComs;

import org.junit.Assert;
import org.junit.Test;

public class SimpleDotComsApplicationTest {

        @Test
        public void testGrid() {
            SimpleDotComs simpleDotComs = new SimpleDotComs();
            Boolean[] grid = simpleDotComs.setLocations();

            Assert.assertEquals(7, grid.length);

            int numberOfTrue = 0;
            int numberOfNull = 0;
            for(Boolean ele : grid) {
                if(ele == null){
                    numberOfNull++;
                } else if(ele){
                    numberOfTrue++;
                }
            }
            Assert.assertEquals(3, numberOfTrue);
            Assert.assertEquals(4, numberOfNull);
            Assert.assertEquals(7, grid.length);
        }

    @Test
    public void testUserInput() {
        SimpleDotComs simpleDotComs = new SimpleDotComs();

        int n = 10;
        Assert.assertEquals(simpleDotComs.checkUserInput(n), false);

        n = -2379;
        Assert.assertEquals(simpleDotComs.checkUserInput(n), false);

        n = 1000;
        Assert.assertEquals(simpleDotComs.checkUserInput(n), false);

        n = 3;
        Assert.assertEquals(simpleDotComs.checkUserInput(n), true);

        n = 7;
        Assert.assertEquals(simpleDotComs.checkUserInput(n), false);

        n = 6;
        Assert.assertEquals(simpleDotComs.checkUserInput(n), true);

        n = 0;
        Assert.assertEquals(simpleDotComs.checkUserInput(n), true);
    }

}
